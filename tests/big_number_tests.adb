-------------------------------------------------------------------------------
-- This file is part of libsparkcrypto.
--
-- Copyright (C) 2010, Reto Buerki
-- Copyright (C) 2010, Adrian-Ken Rueegsegger
-- Copyright (C) 2010, secunet Security Networks AG
-- All rights reserved.
--
-- Redistribution  and  use  in  source  and  binary  forms,  with  or  without
-- modification, are permitted provided that the following conditions are met:
--
--    * Redistributions of source code must retain the above copyright notice,
--      this list of conditions and the following disclaimer.
--
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
--    * Neither the name of the  nor the names of its contributors may be used
--      to endorse or promote products derived from this software without
--      specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE  COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY  EXPRESS OR IMPLIED WARRANTIES,  INCLUDING, BUT NOT LIMITED  TO, THE
-- IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR  A PARTICULAR PURPOSE
-- ARE  DISCLAIMED. IN  NO EVENT  SHALL  THE COPYRIGHT  HOLDER OR  CONTRIBUTORS
-- BE  LIABLE FOR  ANY  DIRECT, INDIRECT,  INCIDENTAL,  SPECIAL, EXEMPLARY,  OR
-- CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT  OF
-- SUBSTITUTE GOODS  OR SERVICES; LOSS  OF USE,  DATA, OR PROFITS;  OR BUSINESS
-- INTERRUPTION)  HOWEVER CAUSED  AND ON  ANY THEORY  OF LIABILITY,  WHETHER IN
-- CONTRACT,  STRICT LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE OR  OTHERWISE)
-- ARISING IN ANY WAY  OUT OF THE USE OF THIS SOFTWARE, EVEN  IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

with Ahven;

with Big_Numbers.Debug;

pragma Elaborate_All (Big_Numbers);

package body Big_Number_Tests is

   use Ahven;
   use Big_Numbers;

   --  397936397349613490120942309457
   Number_1 : constant Big_Unsigned := To_Bignum
     (Bytes =>
        (Byte_Index'Last      => 16#51#,
         Byte_Index'Last - 1  => 16#50#,
         Byte_Index'Last - 2  => 16#6E#,
         Byte_Index'Last - 3  => 16#9D#,
         Byte_Index'Last - 4  => 16#11#,
         Byte_Index'Last - 5  => 16#40#,
         Byte_Index'Last - 6  => 16#13#,
         Byte_Index'Last - 7  => 16#E0#,
         Byte_Index'Last - 8  => 16#DD#,
         Byte_Index'Last - 9  => 16#45#,
         Byte_Index'Last - 10 => 16#CD#,
         Byte_Index'Last - 11 => 16#05#,
         Byte_Index'Last - 12 => 16#05#,
         others               => 0));

   --  36321136410385
   Number_2 : constant Big_Unsigned := To_Bignum
     (Bytes =>
        (Byte_Index'Last      => 16#11#,
         Byte_Index'Last - 1  => 16#FF#,
         Byte_Index'Last - 2  => 16#6E#,
         Byte_Index'Last - 3  => 16#AC#,
         Byte_Index'Last - 4  => 16#08#,
         Byte_Index'Last - 5  => 16#21#,
         others               => 0));

   -------------------------------------------------------------------------

   procedure Add_Bignums
   is
      Num2 : constant Big_Unsigned := To_Bignum
        (Bytes =>
           (Byte_Index'Last     => 16#FF#,
            Byte_Index'Last - 1 => 16#FF#,
            Byte_Index'Last - 2 => 16#FF#,
            Byte_Index'Last - 3 => 16#FF#,
            others              => 0));

      Result_String1 : constant String := "795872794699226980241884618914";
      Result_String2 : constant String := "4294967296";
   begin
      Assert (Condition => Debug.To_String
              (Number => Add
               (Left  => Number_1,
                Right => Number_1)) = Result_String1,
              Message   => "Large addition failed");
      Assert (Condition => Debug.To_String
              (Number => Add
               (Left  => Num2,
                Right => Big_Unsigned_One)) = Result_String2,
              Message   => "Carry error");
   end Add_Bignums;

   -------------------------------------------------------------------------

   procedure Clear_Bignum
   is
      B1 : Big_Unsigned;
   begin
      Clear (Number => B1);
      Assert (Condition => B1 = Big_Unsigned_Zero,
              Message   => "Bignumber not cleared");
   end Clear_Bignum;

   -------------------------------------------------------------------------

   procedure Compare_Bignums
   is
      Small : constant Big_Unsigned := To_Bignum
        (Bytes => (Byte_Index'Last     => 16#01#,
                   Byte_Index'Last - 1 => 16#02#,
                   Byte_Index'Last - 2 => 16#03#,
                   Byte_Index'Last - 3 => 16#04#,
                   Byte_Index'Last - 4 => 16#05#,
                   others              => 0));
      Big   : constant Big_Unsigned := To_Bignum
        (Bytes => (others => 16#03#));
   begin
      Assert (Condition => Less_Than
              (Left  => Small,
               Right => Big),
              Message   => "Small not less than Big");
      Assert (Condition => not Less_Than
              (Left  => Big,
               Right => Small),
              Message   => "Big less than Small");
      Assert (Condition => not Less_Than
              (Left  => Big,
               Right => Big),
              Message   => "Big less than Big");

      Assert (Condition => Greater_Than
              (Left  => Big,
               Right => Small),
              Message   => "Big not greater than Small");
      Assert (Condition => not Greater_Than
              (Left  => Small,
               Right => Big),
              Message   => "Small greater than Big");
      Assert (Condition => not Greater_Than
              (Left  => Big,
               Right => Big),
              Message   => "Big greater than Big");

      Assert (Condition => Equal
              (Left  => Small,
               Right => Small),
              Message   => "Small not equal to self");
      Assert (Condition => Equal
              (Left  => Big,
               Right => Big),
              Message   => "Big not equal to self");
      Assert (Condition => not Equal
              (Left  => Small,
               Right => Big),
              Message   => "Small equal to Big");
   end Compare_Bignums;

   -------------------------------------------------------------------------

   procedure Compare_Bignums_With_Limb
   is
      Num1 : constant Big_Unsigned := To_Bignum
        (Bytes => (others => 16#03#));
      Num2 : constant Big_Unsigned := To_Bignum
        (Bytes  =>
           (Byte_Index'Last => 9,
            others          => 0));
   begin
      Assert (Condition => Equal_Limb
              (Left  => Num2,
               Right => 9),
              Message   => "Num2 not equal to 9");
      Assert (Condition => not Equal_Limb
              (Left  => Num2,
               Right => 21),
              Message   => "Num2 equal to 21");
      Assert (Condition => not Equal_Limb
              (Left  => Num1,
               Right => 3),
              Message   => "Num1 equal to 3");

      Assert (Condition => Less_Than_Limb
              (Left  => Num2,
               Right => 10),
              Message   => "Num2 not less than 10");
      Assert (Condition => not Less_Than_Limb
              (Left  => Num1,
               Right => Limb_Type'Last),
              Message   => "Num1 less than Mod_Type'Last");
   end Compare_Bignums_With_Limb;

   -------------------------------------------------------------------------

   procedure Compute_Bitlength
   is
      Num1 : constant Big_Unsigned := To_Bignum
        (Bytes => (Byte_Index'Last     => 16#01#,
                   Byte_Index'Last - 1 => 16#02#,
                   Byte_Index'Last - 2 => 16#03#,
                   Byte_Index'Last - 3 => 16#04#,
                   Byte_Index'Last - 4 => 16#05#,
                   others              => 0));
      Num2 : constant Big_Unsigned := To_Bignum
        (Bytes => (others => 16#FF#));
      M1   : constant Limb_Type    := 6675;
   begin
      Assert (Condition => Bit_Length_Limb (Number => M1) = 13,
              Message   => "Bitlength not 13");
      Assert (Condition => Bit_Length_Limb (Number => Limb_Type'First) = 0,
              Message   => "Bitlength not 0");
      Assert (Condition => Bit_Length_Limb
              (Number => Limb_Type'Last) = Word_Size,
              Message   => "Bitlength not" & Word_Size'Img);

      Assert (Condition => Bit_Length (Number => Big_Unsigned_Zero) = 0,
              Message   => "Bit length not 0");
      Assert (Condition => Bit_Length (Number => Big_Unsigned_One) = 1,
              Message   => "Bit length not 1");
      Assert (Condition => Bit_Length (Number => Num1) = 35,
              Message   => "Bit length not 35");
      Assert (Condition => Bit_Length (Number => Number_1) = 99,
              Message   => "Bitlength not 99");
      Assert (Condition => Bit_Length (Number => Num2) =
                Limb_Type'Size * (Limb_Index'Last + 1),
              Message   => "Bit length not max");

   end Compute_Bitlength;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for unsigned bignumbers");
      T.Add_Test_Routine
        (Routine => Clear_Bignum'Access,
         Name    => "Clear bignumber object");
      T.Add_Test_Routine
        (Routine => To_Bignum'Access,
         Name    => "Convert bytes to bignumber");
      T.Add_Test_Routine
        (Routine => Compare_Bignums'Access,
         Name    => "Compare bignumbers");
      T.Add_Test_Routine
        (Routine => Compare_Bignums_With_Limb'Access,
         Name    => "Compare bignumbers with limbs");
      T.Add_Test_Routine
        (Routine => Compute_Bitlength'Access,
         Name    => "Compute bit length");
      T.Add_Test_Routine
        (Routine => Add_Bignums'Access,
         Name    => "Add bignums");
      T.Add_Test_Routine
        (Routine => Substract_Bignums'Access,
         Name    => "Substract bignums");
      T.Add_Test_Routine
        (Routine => Multiply_Bignums'Access,
         Name    => "Multiply bignums");
      T.Add_Test_Routine
        (Routine => Mul_Limb_Bignums'Access,
         Name    => "Multiply bignums with limbs");
      T.Add_Test_Routine
        (Routine => Left_Shift_Bignum'Access,
         Name    => "Left shift bignums");
      T.Add_Test_Routine
        (Routine => Short_Div_Bignums'Access,
         Name    => "Short division");
      T.Add_Test_Routine
        (Routine => Long_Div_Bignums'Access,
         Name    => "Long division");
      T.Add_Test_Routine
        (Routine => Modulo_Limb_Bignums'Access,
         Name    => "Bignum modulo limb");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Left_Shift_Bignum
   is
      Result : constant String :=
        "66118524987362081023035067554818217279578479036972140860647931904";
   begin
      Assert (Condition => Shift_Left
              (Number => Big_Unsigned_Zero,
               Amount => 58) = Big_Unsigned_Zero,
              Message   => "Shift of zero not zero");
      Assert (Condition => Shift_Left
              (Number => Number_1,
               Amount => 0) = Number_1,
              Message   => "0 shift not equal Num1");
      Assert (Condition => Shift_Left
              (Number => Big_Unsigned_One,
               Amount => Bignum_Size'Last) = Big_Unsigned_Zero,
              Message   => "Max shift failed");
      Assert (Condition => Debug.To_String
              (Number => Shift_Left
               (Number => Number_1,
                Amount => 117)) = Result,
              Message   => "Shift of Num1 failed");
   end Left_Shift_Bignum;

   -------------------------------------------------------------------------

   procedure Long_Div_Bignums
   is
      Q : Big_Unsigned;
      R : Big_Unsigned;
   begin
      declare
         --  140741783191552
         Dividend : constant Big_Unsigned := To_Bignum
           (Bytes =>
              (Byte_Index'Last      => 16#00#,
               Byte_Index'Last - 1  => 16#00#,
               Byte_Index'Last - 2  => 16#FE#,
               Byte_Index'Last - 3  => 16#FF#,
               Byte_Index'Last - 4  => 16#00#,
               Byte_Index'Last - 5  => 16#80#,
               others               => 0));

         --  2147549183
         Divisor : constant Big_Unsigned := To_Bignum
           (Bytes =>
              (Byte_Index'Last      => 16#FF#,
               Byte_Index'Last - 1  => 16#FF#,
               Byte_Index'Last - 2  => 16#00#,
               Byte_Index'Last - 3  => 16#80#,
               others               => 0));
         Q_Str : constant String := "65535";
         R_Str : constant String := "2147483647";
      begin
         Long_Div (Dividend  => Dividend,
                   Divisor   => Divisor,
                   Quotient  => Q,
                   Remainder => R);
         Assert (Condition => Debug.To_String (Number => Q) = Q_Str,
                 Message   => "1: wrong Q");
         Assert (Condition => Debug.To_String (Number => R) = R_Str,
                 Message   => "1: wrong R");
      end;

      declare
         --  140737488355331
         Dividend : constant Big_Unsigned := To_Bignum
           (Bytes =>
              (Byte_Index'Last      => 16#03#,
               Byte_Index'Last - 1  => 16#00#,
               Byte_Index'Last - 2  => 16#00#,
               Byte_Index'Last - 3  => 16#00#,
               Byte_Index'Last - 4  => 16#00#,
               Byte_Index'Last - 5  => 16#80#,
               others               => 0));

         --  35184372088833
         Divisor : constant Big_Unsigned := To_Bignum
           (Bytes =>
              (Byte_Index'Last      => 16#01#,
               Byte_Index'Last - 1  => 16#00#,
               Byte_Index'Last - 2  => 16#00#,
               Byte_Index'Last - 3  => 16#00#,
               Byte_Index'Last - 4  => 16#00#,
               Byte_Index'Last - 5  => 16#20#,
               others               => 0));
         Q_Str : constant String := "3";
         R_Str : constant String := "35184372088832";
      begin
         Long_Div (Dividend  => Dividend,
                   Divisor   => Divisor,
                   Quotient  => Q,
                   Remainder => R);
         Assert (Condition => Debug.To_String (Number => Q) = Q_Str,
                 Message   => "2: wrong Q");
         Assert (Condition => Debug.To_String (Number => R) = R_Str,
                 Message   => "2: wrong R");
      end;

      declare
         Q_Str : constant String := "10956055803249450";
         R_Str : constant String := "13929216771207";
      begin
         Long_Div (Dividend  => Number_1,
                   Divisor   => Number_2,
                   Quotient  => Q,
                   Remainder => R);
         Assert (Condition => Debug.To_String (Number => Q) = Q_Str,
                 Message   => "3: wrong Q");
         Assert (Condition => Debug.To_String (Number => R) = R_Str,
                 Message   => "3: wrong R");
      end;
   end Long_Div_Bignums;

   -------------------------------------------------------------------------

   procedure Mul_Limb_Bignums
   is
      --  Big_Unsigned_Last / 5
      Big_Max_Fifth : constant Big_Unsigned := To_Bignum
        (Bytes => (others => 16#33#));

      --  3576
      Big_Num1   : constant Big_Unsigned := To_Bignum
        (Bytes => (Byte_Index'Last     => 16#F8#,
                   Byte_Index'Last - 1 => 16#0D#,
                   others              => 0));
      Big_Num2   : constant Big_Unsigned := To_Bignum
        (Bytes => (Byte_Index'Last     => 16#FF#,
                   Byte_Index'Last - 1 => 16#FF#,
                   Byte_Index'Last - 2 => 16#FF#,
                   Byte_Index'Last - 3 => 16#FF#,
                   others              => 0));

      Mod_Num1    : constant Limb_Type := 3576;
      Result_Str2 : constant String    := "8589934590";
      Result_Str1 : constant String    :=
        "1423020556922217840672489698618232";
   begin
      Assert (Condition => Mul_Limb
              (Left  => Number_1,
               Right => 0) = Big_Unsigned_Zero,
              Message   => "Zero multiply failed1");
      Assert (Condition => Mul_Limb
              (Left  => Big_Unsigned_Zero,
               Right => Mod_Num1) = Big_Unsigned_Zero,
              Message   => "Zero multiply failed2");

      Assert (Condition => Mul_Limb
              (Left  => Big_Unsigned_One,
               Right => Mod_Num1) = Big_Num1,
              Message   => "Multiply by one failed1");
      Assert (Condition => Mul_Limb
              (Left  => Big_Num1,
               Right => 1) = Big_Num1,
              Message   => "Multiply by one failed2");

      Assert (Condition => Debug.To_String
              (Number => Mul_Limb
               (Left  => Number_1,
                Right => Mod_Num1)) = Result_Str1,
              Message   => "Multiply failed1");

      Assert (Condition => Mul_Limb
              (Left  => Big_Max_Fifth,
               Right => 5) = Big_Unsigned_Last,
              Message   => "Multiply failed2");
      Assert (Condition => Debug.To_String
              (Number => Mul_Limb
               (Left  => Big_Num2,
                Right => 2)) = Result_Str2,
              Message   => "Multiply failed3");
   end Mul_Limb_Bignums;

   -------------------------------------------------------------------------

   procedure Multiply_Bignums
   is
      Result_String  : constant String :=
        "14453502170792479548438534213224692818510945";
      Result_String2 : constant String :=
        "158353376335589474324875346214901438668470592076792751634849";
   begin
      Assert (Condition => Mul
              (Left  => Number_1,
               Right => Big_Unsigned_Zero) = Big_Unsigned_Zero,
              Message   => "Zero multiply failed1");
      Assert (Condition => Mul
              (Left  => Big_Unsigned_Zero,
               Right => Number_2) = Big_Unsigned_Zero,
              Message   => "Zero multiply failed2");
      Assert (Condition => Mul
              (Left  => Big_Unsigned_One,
               Right => Number_2) = Number_2,
              Message   => "Multiply by one failed1");
      Assert (Condition => Mul
              (Left  => Number_1,
               Right => Big_Unsigned_One) = Number_1,
              Message   => "Multiply by one failed2");

      Assert (Condition => Debug.To_String
              (Number => Mul
               (Left  => Number_1,
                Right => Number_2)) = Result_String,
              Message   => "Multiply failed1");
      Assert (Condition => Debug.To_String
              (Number => Mul
               (Left  => Number_2,
                Right => Number_1)) = Result_String,
              Message   => "Multiply failed2");
      Assert (Condition => Debug.To_String
              (Number => Mul
               (Left  => Number_1,
                Right => Number_1)) = Result_String2,
              Message   => "Multiply failed3");
   end Multiply_Bignums;

   -------------------------------------------------------------------------

   procedure Modulo_Limb_Bignums
   is
      --  2202981957
      Rem_1 : constant Big_Unsigned := To_Bignum
        (Bytes =>
           (Byte_Index'Last     => 16#45#,
            Byte_Index'Last - 1 => 16#D6#,
            Byte_Index'Last - 2 => 16#4E#,
            Byte_Index'Last - 3 => 16#83#,
            others              => 0));
      --  753
      Rem_2 : constant Big_Unsigned := To_Bignum
        (Bytes =>
           (Byte_Index'Last     => 16#F1#,
            Byte_Index'Last - 1 => 16#02#,
            others              => 0));
   begin
      Assert (Condition => Mod_Limb
              (Left  => Number_1,
               Right => 1) = Big_Unsigned_Zero,
              Message   => "Remainder not zero");
      Assert (Condition => Mod_Limb
              (Left  => Number_2,
               Right => 2) = Big_Unsigned_One,
              Message   => "Remainder not one");

      Assert (Condition => Mod_Limb
              (Left  => Number_2,
               Right => 1231) = Rem_2,
              Message   => "Incorrect remainder1");
      Assert (Condition => Mod_Limb
              (Left  => Number_1,
               Right => Limb_Type'Last) = Rem_1,
              Message   => "Incorrect remainder2");
   end Modulo_Limb_Bignums;

   -------------------------------------------------------------------------

   procedure Short_Div_Bignums
   is
      Q : Big_Unsigned;
      R : Limb_Type;
   begin
      Short_Div (Dividend  => Number_1,
                 Divisor   => 1,
                 Quotient  => Q,
                 Remainder => R);
      Assert (Condition => Q = Number_1,
              Message   => "1: Q not Num1");
      Assert (Condition => R = 0,
              Message   => "1: R not 0");

      Short_Div (Dividend  => Big_Unsigned_One,
                 Divisor   => 255,
                 Quotient  => Q,
                 Remainder => R);
      Assert (Condition => Q = Big_Unsigned_Zero,
              Message   => "2: Q not 0");
      Assert (Condition => R = 1,
              Message   => "2: R not 1");

      Short_Div (Dividend  => Big_Unsigned_One,
                 Divisor   => 1,
                 Quotient  => Q,
                 Remainder => R);
      Assert (Condition => Q = Big_Unsigned_One,
              Message   => "3: Q not 1");
      Assert (Condition => R = 0,
              Message   => "3: R not 0");

      declare
         Result : constant String := "1665005846651102469125281629";
      begin
         Short_Div (Dividend  => Number_1,
                    Divisor   => 239,
                    Quotient  => Q,
                    Remainder => R);
         Assert (Condition => Debug.To_String (Number => Q) = Result,
                 Message   => "4: wrong Q");
         Assert (Condition => R = 126,
                 Message   => "4: R not 126");
      end;
   end Short_Div_Bignums;

   -------------------------------------------------------------------------

   procedure Substract_Bignums
   is
      --  157521824386092479846092374015
      Num2 : constant Big_Unsigned := To_Bignum
        (Bytes =>
           (Byte_Index'Last      => 16#FF#,
            Byte_Index'Last - 1  => 16#FF#,
            Byte_Index'Last - 2  => 16#FF#,
            Byte_Index'Last - 3  => 16#FF#,
            Byte_Index'Last - 4  => 16#AB#,
            Byte_Index'Last - 5  => 16#12#,
            Byte_Index'Last - 6  => 16#01#,
            Byte_Index'Last - 7  => 16#AA#,
            Byte_Index'Last - 8  => 16#CA#,
            Byte_Index'Last - 9  => 16#FF#,
            Byte_Index'Last - 10 => 16#FA#,
            Byte_Index'Last - 11 => 16#FC#,
            Byte_Index'Last - 12 => 16#01#,
            others               => 0));

      Result_String : constant String := "240414572963521010274849935442";
   begin
      Assert (Condition => Sub
              (Left  => Num2,
               Right => Num2) = Big_Unsigned_Zero,
              Message   => "Num2 - Num2 not 0");
      Assert (Condition => Debug.To_String
              (Number => Sub
               (Left  => Number_1,
                Right => Num2)) = Result_String,
              Message   => "Incorrect result for Num1 - Num2");
   end Substract_Bignums;

   -------------------------------------------------------------------------

   procedure To_Bignum
   is
      Num         : Big_Unsigned;
      Bytes       : Byte_Array := (others => 0);
      Ref_String1 : constant String := "44287";
      Ref_String2 : constant String := "397936397349613490120942309457";
   begin
      Bytes (Byte_Index'Last)     := 16#FF#;
      Bytes (Byte_Index'Last - 1) := 16#AC#;
      Num := To_Bignum (Bytes => Bytes);

      Assert (Condition => Debug.To_String (Number => Num) = Ref_String1,
              Message   => "Conversion error 1");

      Bytes (Byte_Index'Last)      := 16#51#;
      Bytes (Byte_Index'Last - 1)  := 16#50#;
      Bytes (Byte_Index'Last - 2)  := 16#6E#;
      Bytes (Byte_Index'Last - 3)  := 16#9D#;
      Bytes (Byte_Index'Last - 4)  := 16#11#;
      Bytes (Byte_Index'Last - 5)  := 16#40#;
      Bytes (Byte_Index'Last - 6)  := 16#13#;
      Bytes (Byte_Index'Last - 7)  := 16#E0#;
      Bytes (Byte_Index'Last - 8)  := 16#DD#;
      Bytes (Byte_Index'Last - 9)  := 16#45#;
      Bytes (Byte_Index'Last - 10) := 16#CD#;
      Bytes (Byte_Index'Last - 11) := 16#05#;
      Bytes (Byte_Index'Last - 12) := 16#05#;

      Num := To_Bignum (Bytes => Bytes);

      Assert (Condition => Debug.To_String (Number => Num) = Ref_String2,
              Message   => "Conversion error 2");
   end To_Bignum;

end Big_Number_Tests;
