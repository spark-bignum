-------------------------------------------------------------------------------
-- This file is part of libsparkcrypto.
--
-- Copyright (C) 2010, Reto Buerki
-- Copyright (C) 2010, Adrian-Ken Rueegsegger
-- Copyright (C) 2010, secunet Security Networks AG
-- All rights reserved.
--
-- Redistribution  and  use  in  source  and  binary  forms,  with  or  without
-- modification, are permitted provided that the following conditions are met:
--
--    * Redistributions of source code must retain the above copyright notice,
--      this list of conditions and the following disclaimer.
--
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
--    * Neither the name of the  nor the names of its contributors may be used
--      to endorse or promote products derived from this software without
--      specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE  COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY  EXPRESS OR IMPLIED WARRANTIES,  INCLUDING, BUT NOT LIMITED  TO, THE
-- IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR  A PARTICULAR PURPOSE
-- ARE  DISCLAIMED. IN  NO EVENT  SHALL  THE COPYRIGHT  HOLDER OR  CONTRIBUTORS
-- BE  LIABLE FOR  ANY  DIRECT, INDIRECT,  INCIDENTAL,  SPECIAL, EXEMPLARY,  OR
-- CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT  OF
-- SUBSTITUTE GOODS  OR SERVICES; LOSS  OF USE,  DATA, OR PROFITS;  OR BUSINESS
-- INTERRUPTION)  HOWEVER CAUSED  AND ON  ANY THEORY  OF LIABILITY,  WHETHER IN
-- CONTRACT,  STRICT LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE OR  OTHERWISE)
-- ARISING IN ANY WAY  OUT OF THE USE OF THIS SOFTWARE, EVEN  IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

with Big_Numbers;

with SPARKUnit;

--# inherit
--#    Big_Numbers,
--#    SPARKUnit,
--#    SPARK_IO;

--# main_program;
procedure Main
--# global in out SPARK_IO.Outputs;
--# derives SPARK_IO.Outputs from *;
is
   subtype Harness_Index is Natural range 1 .. 100;
   subtype Harness_Type is SPARKUnit.Harness_Type (Harness_Index);

   Harness    : Harness_Type;
   Benchmarks : SPARKUnit.Index_Type;

   subtype Iterations is Natural range 1 .. 1000000;

   Number_1_Str : constant String := "397936397349613490120942309457";
   Number_2_Str : constant String := "36321136410385";

   function Get_BN1 return Big_Numbers.Big_Unsigned
   is
   begin
      return Big_Numbers.To_Bignum
        (Bytes => Big_Numbers.Byte_Array'
           (Big_Numbers.Byte_Index'Last      => 16#51#,
            Big_Numbers.Byte_Index'Last - 1  => 16#50#,
            Big_Numbers.Byte_Index'Last - 2  => 16#6E#,
            Big_Numbers.Byte_Index'Last - 3  => 16#9D#,
            Big_Numbers.Byte_Index'Last - 4  => 16#11#,
            Big_Numbers.Byte_Index'Last - 5  => 16#40#,
            Big_Numbers.Byte_Index'Last - 6  => 16#13#,
            Big_Numbers.Byte_Index'Last - 7  => 16#E0#,
            Big_Numbers.Byte_Index'Last - 8  => 16#DD#,
            Big_Numbers.Byte_Index'Last - 9  => 16#45#,
            Big_Numbers.Byte_Index'Last - 10 => 16#CD#,
            Big_Numbers.Byte_Index'Last - 11 => 16#05#,
            Big_Numbers.Byte_Index'Last - 12 => 16#05#,
            others                           => 0));
   end Get_BN1;

   function Get_BN2 return Big_Numbers.Big_Unsigned
   is
   begin
      return Big_Numbers.To_Bignum
        (Bytes => Big_Numbers.Byte_Array'
           (Big_Numbers.Byte_Index'Last      => 16#11#,
            Big_Numbers.Byte_Index'Last - 1  => 16#FF#,
            Big_Numbers.Byte_Index'Last - 2  => 16#6E#,
            Big_Numbers.Byte_Index'Last - 3  => 16#AC#,
            Big_Numbers.Byte_Index'Last - 4  => 16#08#,
            Big_Numbers.Byte_Index'Last - 5  => 16#21#,
            others                           => 0));
   end Get_BN2;

   procedure Bignum_Addition
   --# global Harness, Benchmarks;
   --# derives Harness from Harness, Benchmarks;
   is separate;

   procedure Bignum_Substraction
   --# global Harness, Benchmarks;
   --# derives Harness from Harness, Benchmarks;
   is separate;

   procedure Bignum_Multiplication_Mod
   --# global Harness, Benchmarks;
   --# derives Harness from Harness, Benchmarks;
   is separate;

   procedure Bignum_Multiplication
   --# global Harness, Benchmarks;
   --# derives Harness from Harness, Benchmarks;
   is separate;

   procedure Bignum_Division
   --# global Harness, Benchmarks;
   --# derives Harness from Harness, Benchmarks;
   is separate;

   procedure Bignum_Remainder_Mod
   --# global Harness, Benchmarks;
   --# derives Harness from Harness, Benchmarks;
   is separate;

begin

   SPARKUnit.Create_Harness (Harness, "spark-bignum tests");
   SPARKUnit.Create_Suite (Harness, "Benchmarks", Benchmarks);

   Bignum_Addition;
   Bignum_Substraction;
   Bignum_Multiplication_Mod;
   Bignum_Multiplication;
   Bignum_Division;
   Bignum_Remainder_Mod;

   SPARKUnit.Text_Report (Harness);

end Main;
