-------------------------------------------------------------------------------
-- This file is part of libsparkcrypto.
--
-- Copyright (C) 2010, Reto Buerki
-- Copyright (C) 2010, Adrian-Ken Rueegsegger
-- Copyright (C) 2010, secunet Security Networks AG
-- All rights reserved.
--
-- Redistribution  and  use  in  source  and  binary  forms,  with  or  without
-- modification, are permitted provided that the following conditions are met:
--
--    * Redistributions of source code must retain the above copyright notice,
--      this list of conditions and the following disclaimer.
--
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
--    * Neither the name of the  nor the names of its contributors may be used
--      to endorse or promote products derived from this software without
--      specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE  COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY  EXPRESS OR IMPLIED WARRANTIES,  INCLUDING, BUT NOT LIMITED  TO, THE
-- IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR  A PARTICULAR PURPOSE
-- ARE  DISCLAIMED. IN  NO EVENT  SHALL  THE COPYRIGHT  HOLDER OR  CONTRIBUTORS
-- BE  LIABLE FOR  ANY  DIRECT, INDIRECT,  INCIDENTAL,  SPECIAL, EXEMPLARY,  OR
-- CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT  OF
-- SUBSTITUTE GOODS  OR SERVICES; LOSS  OF USE,  DATA, OR PROFITS;  OR BUSINESS
-- INTERRUPTION)  HOWEVER CAUSED  AND ON  ANY THEORY  OF LIABILITY,  WHETHER IN
-- CONTRACT,  STRICT LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE OR  OTHERWISE)
-- ARISING IN ANY WAY  OUT OF THE USE OF THIS SOFTWARE, EVEN  IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

with Interfaces;

package SR is

   function Shift_Left_8
     (Value  : Interfaces.Unsigned_8;
      Amount : Natural)
      return Interfaces.Unsigned_8
      renames Interfaces.Shift_Left;

   function Shift_Left_16
     (Value  : Interfaces.Unsigned_16;
      Amount : Natural)
      return Interfaces.Unsigned_16
      renames Interfaces.Shift_Left;

   function Shift_Left_32
     (Value  : Interfaces.Unsigned_32;
      Amount : Natural)
      return Interfaces.Unsigned_32
      renames Interfaces.Shift_Left;

   function Shift_Left_64
     (Value  : Interfaces.Unsigned_64;
      Amount : Natural)
      return Interfaces.Unsigned_64
      renames Interfaces.Shift_Left;

   function Shift_Right_8
     (Value  : Interfaces.Unsigned_8;
      Amount : Natural)
      return Interfaces.Unsigned_8
      renames Interfaces.Shift_Right;

   function Shift_Right_16
     (Value  : Interfaces.Unsigned_16;
      Amount : Natural)
      return Interfaces.Unsigned_16
      renames Interfaces.Shift_Right;

   function Shift_Right_32
     (Value  : Interfaces.Unsigned_32;
      Amount : Natural)
      return Interfaces.Unsigned_32
      renames Interfaces.Shift_Right;

   function Shift_Right_64
     (Value  : Interfaces.Unsigned_64;
      Amount : Natural)
      return Interfaces.Unsigned_64
      renames Interfaces.Shift_Right;

end SR;
