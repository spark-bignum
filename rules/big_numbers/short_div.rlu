modulus_property(1):
   (D + sr__shift_left_64(C, 32)) mod 18446744073709551616 div R <= 4294967295
   may_be_deduced_from
   [/* H1: */    C < R,
    /* H2: */    0 <= D,
    /* H3: */    D <= 4294967295,
    /* H4: */    R <= 4294967295,
    /* H5: */    C >= 0,
    /* H6: */    sr__shift_left_64(C, 32) = C * 4294967296].


/* Justification:
   --------------

   First, using H6, rewrite the goal to prove to:

   Goal: (D + C * 4294967296) mod 18446744073709551616 div R <= 4294967295

   Next, we will establish that D + C * 4294967296 <= 18446744073709551615,
   allowing us to eliminate the modulus expression in our Goal formula.

   Subgoal: D + C * 4294967296 <= 18446744073709551615

      From H1 and H4, C <= 4294967295, from which we can infer

      Ha:  C * 4294967296 <= 4294967295 * 4294967296

      i.e. (simplifying):

      Ha:  C * 4294967296 <= 18446744069414584320.

      Adding H3 and Ha and simplifying, this gives

      Hb:  D + C * 4294967296 <= 18446744073709551615.

      This is sufficient to prove our subgoal.

   Subgoal proved, giving new top-level hypothesis,

   H7:  D + C * 4294967296 <= 18446744073709551615

   Given H7, we can now rewrite our goal formula (using Proof Checker rule
   modular(41)) to:

   Goal: (D + C * 4294967296) div R <= 4294967295

   We will prove this by contradiction, in which we assume the negation
   of the goal, and prove that this results in a contradiction with the
   other hypotheses.

   Subgoal: false

      Ha:  4294967296 <= (D + C * 4294967296) div R

      (This is the negation of the main goal, after simplification.)

      Multiplying both sides of Ha by R (legal, given R>0 from H1, H5), we get

      Hb:  4294967296 * R <= (D + C * 4294967296) div R * R.

      From H2 and H5, it follows that D + C * 4294967296 >= 0, and from
      H1 and H5, it follows that R <> 0, so we may use Proof Checker rule
      inequals(89) to infer

      Hc:  (D + C * 4294967296) div R * R <= D + C * 4294967296.

      Using transitivity om Hb and Hc, we get

      Hd:  4294967296 * R <= D + C * 4294967296.

      Now, from H1, it follows that C + 1 <= R, and we can multiply both sides
      of this by 4294967296 to get

      He:  (C + 1) * 4294967296 <= R * 4294967296.

      Next, using transitivity on He and Hd yields

      Hf:  (C + 1) * 4294967296 <= D + C * 4294967296

      which, on eliminiating C * D + C * 4294967296 from both sides, becomes

      Hf:  4294967296 <= D.

      But this contradicts H3, completing the proof by contradiction.

   Subgoal proved, establishing top-level goal formula.  QED.

*/

