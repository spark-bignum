-------------------------------------------------------------------------------
-- This file is part of libsparkcrypto.
--
-- Copyright (C) 2010, Reto Buerki
-- Copyright (C) 2010, Adrian-Ken Rueegsegger
-- Copyright (C) 2010, secunet Security Networks AG
-- All rights reserved.
--
-- Redistribution  and  use  in  source  and  binary  forms,  with  or  without
-- modification, are permitted provided that the following conditions are met:
--
--    * Redistributions of source code must retain the above copyright notice,
--      this list of conditions and the following disclaimer.
--
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
--    * Neither the name of the  nor the names of its contributors may be used
--      to endorse or promote products derived from this software without
--      specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE  COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY  EXPRESS OR IMPLIED WARRANTIES,  INCLUDING, BUT NOT LIMITED  TO, THE
-- IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR  A PARTICULAR PURPOSE
-- ARE  DISCLAIMED. IN  NO EVENT  SHALL  THE COPYRIGHT  HOLDER OR  CONTRIBUTORS
-- BE  LIABLE FOR  ANY  DIRECT, INDIRECT,  INCIDENTAL,  SPECIAL, EXEMPLARY,  OR
-- CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT  OF
-- SUBSTITUTE GOODS  OR SERVICES; LOSS  OF USE,  DATA, OR PROFITS;  OR BUSINESS
-- INTERRUPTION)  HOWEVER CAUSED  AND ON  ANY THEORY  OF LIABILITY,  WHETHER IN
-- CONTRACT,  STRICT LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE OR  OTHERWISE)
-- ARISING IN ANY WAY  OUT OF THE USE OF THIS SOFTWARE, EVEN  IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

with System;
with Interfaces;

with SR;

--# inherit SR,
--#         System,
--#         Interfaces;

package Big_Numbers is

   --  Byte

   type Byte is mod 2 ** 8;
   for Byte'Size use 8;

   subtype Byte_Index is Natural range 0 .. 127;

   type Byte_Array is array (Byte_Index) of Byte;

   --  Bignums

   Word_Size : constant := 32;

   type Limb_Type is mod 2 ** Word_Size;
   for Limb_Type'Size use Word_Size;

   Bytes_Per_Limb : constant := Limb_Type'Size / Byte'Size;

   subtype Limb_Index is Byte_Index range
     Byte_Index'First .. (Byte_Array'Length / Bytes_Per_Limb) - 1;

   type Limb_Array is array (Limb_Index) of Limb_Type;

   --  Size of Bignumber in bits.
   subtype Bignum_Size is Natural range
     0 .. Limb_Type'Size * (Limb_Index'Last + 1);

   --  Size of Limb in bits.
   subtype Limb_Bit_Size is Bignum_Size range
     Bignum_Size'First .. Limb_Type'Size;

   --  Limb type range extended by radix (2 ** Word_Size).
   type Radix_Type is range Limb_Type'First .. 2 ** Word_Size;

   --  Unsigned Bignumber type.
   type Big_Unsigned is private;

   Big_Unsigned_Zero : constant Big_Unsigned;
   Big_Unsigned_One  : constant Big_Unsigned;
   Big_Unsigned_Two  : constant Big_Unsigned;
   Big_Unsigned_Last : constant Big_Unsigned;

   --  Clear a Bignumber object.
   procedure Clear (Number : out Big_Unsigned);
   --# derives Number from ;

   --  Return maximum value of two given bit length values.
   function Max_Bit_Length (Left, Right : Bignum_Size) return Bignum_Size;
   --#  return Result =>
   --#     (Left  <  Right -> Result = Right) and
   --#     (Right <= Left  -> Result = Left);

   --  Return maximum value of two given Limb indexes.
   function Max_Limb_Index (Left, Right : Limb_Index) return Limb_Index;
   --#  return Result =>
   --#     (Left  <  Right -> Result = Right) and
   --#     (Right <= Left  -> Result = Left);

   --  Return maximum value of two given Limbs.
   function Max_Limb (Left, Right : Limb_Type) return Limb_Type;
   --#  return Result =>
   --#     (Left  <  Right -> Result = Right) and
   --#     (Right <= Left  -> Result = Left);

   --  Return minimum value of two given Limb indexes.
   function Min_Limb_Index (Left, Right : Limb_Index) return Limb_Index;
   --#  return Result =>
   --#     (Left  <  Right -> Result = Left)  and
   --#     (Right <= Left  -> Result = Right) and
   --#     Result <= Left                     and
   --#     Result <= Right;

   --  Compare two Bignumbers.
   function Equal (Left, Right : Big_Unsigned) return Boolean;

   --  Compare Bignumber with Limb.
   function Equal_Limb
     (Left  : Big_Unsigned;
      Right : Limb_Type)
      return Boolean;

   --  Less than function for unsigned Bignumbers (Left < Right).
   function Less_Than (Left, Right : Big_Unsigned) return Boolean;

   --  Less than function for Bignumber < Limb.
   function Less_Than_Limb
     (Left  : Big_Unsigned;
      Right : Limb_Type)
      return Boolean;

   --  Greater than function for unsigned Bignumbers (Left > Right).
   function Greater_Than (Left, Right : Big_Unsigned) return Boolean;

   --  Convert array of bytes to Bignumber.
   function To_Bignum (Bytes : Byte_Array) return Big_Unsigned;

   --  Short division of Bignumber by a Limb_Type.
   procedure Short_Div
     (Dividend  :     Big_Unsigned;
      Divisor   :     Limb_Type;
      Quotient  : out Big_Unsigned;
      Remainder : out Limb_Type);
   --# derives Quotient,
   --#         Remainder from Dividend,
   --#                        Divisor;
   --# pre
   --#    Divisor /= 0;

   --  Division of Bignumber by a Bignumber (Knuth, 4.3.1, Algorithm D).
   procedure Long_Div
     (Dividend  :     Big_Unsigned;
      Divisor   :     Big_Unsigned;
      Quotient  : out Big_Unsigned;
      Remainder : out Big_Unsigned);
   --# derives Quotient,
   --#         Remainder from Dividend,
   --#                        Divisor;
   --# pre
   --#    Divisor /= Big_Unsigned_Zero;

   --  Number of bits occupied by Bignumber.
   function Bit_Length (Number : Big_Unsigned) return Bignum_Size;

   --  Number of bits occupied by a limb.
   function Bit_Length_Limb (Number : Limb_Type) return Limb_Bit_Size;
   --# return Result =>
   --#    (Number  = 0 -> Result = 0) and
   --#    (Number /= 0 ->
   --#       (Result > 0                                    and
   --#       Radix_Type'(2 ** Result) > Radix_Type (Number) and
   --#       Limb_Type'(2 ** (Result - 1)) <= Number));

   --  Addition operation for Big_Unsigned.
   function Add (Left, Right : Big_Unsigned) return Big_Unsigned;
   --# pre
   --#    Max_Bit_Length
   --#       (Left  => Bit_Length (Number => Left),
   --#        Right => Bit_Length (Number => Right)) < Bignum_Size'Last;

   --  Substraction for Big_Unsigned (Left - Right).
   function Sub (Left, Right : Big_Unsigned) return Big_Unsigned;
   --# pre
   --#    Left = Right or
   --#    not Less_Than
   --#       (Left  => Left,
   --#        Right => Right);

   --  Multiply a Bignumber with a Limb.
   function Mul_Limb
     (Left  : Big_Unsigned;
      Right : Limb_Type)
      return Big_Unsigned;
   --# pre
   --#    Bit_Length (Number => Left) + Bit_Length_Limb (Number => Right)
   --#       < Bignum_Size'Last;

   --  Multiply two Bignumbers (Left * Right).
   function Mul (Left, Right : Big_Unsigned) return Big_Unsigned;
   --# pre
   --#    Bit_Length (Number => Left) + Bit_Length (Number => Right)
   --#       < Bignum_Size'Last;

   --  Left shift Bignumber by Amount bits.
   function Shift_Left
     (Number : Big_Unsigned;
      Amount : Bignum_Size)
      return Big_Unsigned;
   --# return Result =>
   --#    (Amount = Bignum_Size'First -> Result = Number) and
   --#    (Amount = Bignum_Size'Last  -> Result = Big_Unsigned_Zero) and
   --#    (Number = Big_Unsigned_Zero -> Result = Big_Unsigned_Zero);
   --  INCOMPLETE

   --  Returns Left mod Right.
   function Mod_Limb
     (Left  : Big_Unsigned;
      Right : Limb_Type)
      return Big_Unsigned;
   --# pre Right /= 0;

private

   type Big_Unsigned is record
      Last_Index : Limb_Index;
      Limbs      : Limb_Array;
   end record;

   Big_Unsigned_Zero : constant Big_Unsigned :=
     Big_Unsigned'(Last_Index => 0,
                   Limbs      => Limb_Array'(others => 0));

   Big_Unsigned_One  : constant Big_Unsigned :=
     Big_Unsigned'(Last_Index => 0,
                   Limbs      => Limb_Array'
                     (0      => 1,
                      others => 0));

   Big_Unsigned_Two  : constant Big_Unsigned :=
     Big_Unsigned'(Last_Index => 0,
                   Limbs      => Limb_Array'
                     (0      => 2,
                      others => 0));

   Big_Unsigned_Last : constant Big_Unsigned :=
     Big_Unsigned'(Last_Index => Limb_Index'Last,
                   Limbs      => Limb_Array'
                     (others => Limb_Type'Last));

   --  Set last index of given Bignumber.
   procedure Set_Last_Index (Number : in out Big_Unsigned);
   --# derives Number from *;

   --  Large unsigned type.
   type LU is mod 2 ** (2 * Word_Size);

   --  Large signed type.
   type LS is range -2 ** (2 * Word_Size - 1) .. 2 ** (2 * Word_Size - 1) - 1;

   subtype Limb_Index_Plus_One is Natural range
     Limb_Index'First .. Limb_Index'Last + 1;

   type Limb_Array_Plus_One is array (Limb_Index_Plus_One) of Limb_Type;

end Big_Numbers;
