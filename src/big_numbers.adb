-------------------------------------------------------------------------------
-- This file is part of libsparkcrypto.
--
-- Copyright (C) 2010, Reto Buerki
-- Copyright (C) 2010, Adrian-Ken Rueegsegger
-- Copyright (C) 2010, secunet Security Networks AG
-- All rights reserved.
--
-- Redistribution  and  use  in  source  and  binary  forms,  with  or  without
-- modification, are permitted provided that the following conditions are met:
--
--    * Redistributions of source code must retain the above copyright notice,
--      this list of conditions and the following disclaimer.
--
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
--    * Neither the name of the  nor the names of its contributors may be used
--      to endorse or promote products derived from this software without
--      specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE  COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY  EXPRESS OR IMPLIED WARRANTIES,  INCLUDING, BUT NOT LIMITED  TO, THE
-- IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR  A PARTICULAR PURPOSE
-- ARE  DISCLAIMED. IN  NO EVENT  SHALL  THE COPYRIGHT  HOLDER OR  CONTRIBUTORS
-- BE  LIABLE FOR  ANY  DIRECT, INDIRECT,  INCIDENTAL,  SPECIAL, EXEMPLARY,  OR
-- CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT  OF
-- SUBSTITUTE GOODS  OR SERVICES; LOSS  OF USE,  DATA, OR PROFITS;  OR BUSINESS
-- INTERRUPTION)  HOWEVER CAUSED  AND ON  ANY THEORY  OF LIABILITY,  WHETHER IN
-- CONTRACT,  STRICT LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE OR  OTHERWISE)
-- ARISING IN ANY WAY  OUT OF THE USE OF THIS SOFTWARE, EVEN  IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

package body Big_Numbers is

   function "or"
     (Left, Right : Interfaces.Unsigned_32)
      return Interfaces.Unsigned_32
      renames Interfaces."or";

   function "xor"
     (Left, Right : Interfaces.Unsigned_32)
      return Interfaces.Unsigned_32
      renames Interfaces."xor";

   function Shift_Right_Arithmetic
     (Value  : LS;
      Amount : Natural)
      return LS;
   --# return Value / 2 ** Amount;
   pragma Import (Intrinsic, Shift_Right_Arithmetic);

   -------------------------------------------------------------------------

   procedure Set_Last_Index (Number : in out Big_Unsigned)
   --# post
   --#    ((Number.Last_Index < Limb_Index'Last and
   --#    Number.Limbs (Number.Last_Index) /= 0)
   --#    -> (for all M in Limb_Index range Number.Last_Index + 1 ..
   --#       Limb_Index'Last => (Number.Limbs (M) = 0)))
   --#  and
   --#    ((Number.Last_Index = 0 and Number.Limbs (Number.Last_Index) = 0)
   --#    -> (for all M in Limb_Index => (Number.Limbs (M) = 0)))
   --#  and
   --#    (Number.Limbs (Limb_Index'Last) /= 0
   --#    <-> Number.Last_Index = Limb_Index'Last);
   is
   begin
      Number.Last_Index := 0;

      for I in reverse Limb_Index loop
         if Number.Limbs (I) /= 0 then
            Number.Last_Index := I;
            exit;
         end if;

         --# assert
         --#    Number.Limbs (I) = 0   and
         --#    Number.Last_Index = 0 and
         --#    (for all M in Limb_Index range I .. Limb_Index'Last
         --#       => (Number.Limbs (M) = 0));

      end loop;
   end Set_Last_Index;

   -------------------------------------------------------------------------

   function Bit_Length_Limb (Number : Limb_Type) return Limb_Bit_Size
   is
      Result : Limb_Bit_Size;
   begin
      if Number = 0 then
         Result := 0;
      else
         Result := 1;

         for I in reverse Limb_Bit_Size range
           Limb_Bit_Size'First .. Limb_Bit_Size'Last - 1
         loop
            if Limb_Type (SR.Shift_Left_32
              (Value  => Interfaces.Unsigned_32 (1),
               Amount => I)) <= Number
            then
               Result := I + 1;

               --# assert
               --#    Number /= 0                             and
               --#    Result > 0                              and
               --#    Limb_Type'(2 ** (Result - 1)) <= Number and
               --#    Radix_Type'(2 ** (Result)) > Radix_Type (Number);

               exit;
            end if;

            --# assert
            --#    Result  = 1 and
            --#    Number /= 0 and
            --#    Limb_Type'(1 * (2 ** I)) > Number;

         end loop;
      end if;

      return Result;
   end Bit_Length_Limb;

   -------------------------------------------------------------------------

   function Bit_Length (Number : Big_Unsigned) return Bignum_Size
   --# return Result =>
   --#    (Number = Big_Unsigned_Zero  -> Result = 0) and
   --#    (Number /= Big_Unsigned_Zero -> Result =
   --#      Number.Last_Index * Limb_Type'Size + Bit_Length_Limb
   --#      (Number.Limbs (Number.Last_Index)));
   is
      Result : Bignum_Size;
   begin
      if Number = Big_Unsigned_Zero then
         Result := 0;
      else
         Result := (Number.Last_Index * Limb_Type'Size)
           + Bit_Length_Limb (Number => Number.Limbs (Number.Last_Index));
      end if;

      return Result;
   end Bit_Length;

   -------------------------------------------------------------------------

   function Max_Bit_Length (Left, Right : Bignum_Size) return Bignum_Size
   is
      Result : Bignum_Size;
   begin
      if Left < Right then
         Result := Right;
      else
         Result := Left;
      end if;

      return Result;
   end Max_Bit_Length;

   -------------------------------------------------------------------------

   function Max_Limb (Left, Right : Limb_Type) return Limb_Type
   is
      Result : Limb_Type;
   begin
      if Left < Right then
         Result := Right;
      else
         Result := Left;
      end if;

      return Result;
   end Max_Limb;

   -------------------------------------------------------------------------

   function Max_Limb_Index (Left, Right : Limb_Index) return Limb_Index
   is
      Result : Limb_Index;
   begin
      if Left < Right then
         Result := Right;
      else
         Result := Left;
      end if;

      return Result;
   end Max_Limb_Index;

   -------------------------------------------------------------------------

   function Min_Limb_Index (Left, Right : Limb_Index) return Limb_Index
   is
      Result : Limb_Index;
   begin
      if Left <= Right then
         Result := Left;
      else
         Result := Right;
      end if;

      return Result;
   end Min_Limb_Index;

   -------------------------------------------------------------------------

   function Add (Left, Right : Big_Unsigned) return Big_Unsigned
   is
      Result     : Big_Unsigned := Big_Unsigned_Zero;
      Carry      : Limb_Type    := 0;
      Bits       : Bignum_Size;
      Last_Index : Limb_Index;
   begin
      Bits := Max_Bit_Length
        (Left  => Bit_Length (Number => Left),
         Right => Bit_Length (Number => Right));

      if Left = Big_Unsigned_Zero then
         Result := Right;
      elsif Right = Big_Unsigned_Zero then
         Result := Left;
      elsif Bits + 1 <= Limb_Type'Size then
         Result.Limbs (0) := Left.Limbs (0) + Right.Limbs (0);
      else
         Last_Index := Max_Limb_Index
           (Left  => Left.Last_Index,
            Right => Right.Last_Index);

         for I in Limb_Index range Limb_Index'First .. Last_Index loop
            Result.Limbs (I) := Left.Limbs (I) + Right.Limbs (I) + Carry;

            if Result.Limbs (I) < Max_Limb
              (Left  => Left.Limbs (I),
               Right => Right.Limbs (I))
            then
               Carry := 1;
            else
               Carry := 0;
            end if;
         end loop;

         if Carry = 1 and then Last_Index < Limb_Index'Last then
            Result.Limbs (Last_Index + 1) := Carry;
            Result.Last_Index             := Last_Index + 1;
         else
            Set_Last_Index (Number => Result);
         end if;
      end if;

      return Result;
   end Add;

   -------------------------------------------------------------------------

   procedure Clear (Number : out Big_Unsigned)
   --# post
   --#    Number.Last_Index = 0 and
   --#    (for all M in Limb_Index range
   --#       Limb_Index'First .. Limb_Index'Last => (Number.Limbs (M) = 0));
   is
   begin
      Number := Big_Unsigned'
        (Last_Index => 0,
         Limbs      => Limb_Array'(others => 0));
   end Clear;

   -------------------------------------------------------------------------

   function Equal (Left, Right : Big_Unsigned) return Boolean
   --# return
   --#   (Left.Last_Index = Right.Last_Index) and
   --#   (Left.Limbs = Right.Limbs);
   is
   begin
      return Left.Last_Index = Right.Last_Index
        and then Left.Limbs = Right.Limbs;
   end Equal;

   -------------------------------------------------------------------------

   function Equal_Limb
     (Left  : Big_Unsigned;
      Right : Limb_Type)
      return Boolean
   --# return
   --#    (Left.Last_Index = 0 and Left.Limbs (0) = Right);
   is
   begin
      return Left.Last_Index = 0 and Left.Limbs (0) = Right;
   end Equal_Limb;

   -------------------------------------------------------------------------

   function Less_Than (Left, Right : Big_Unsigned) return Boolean
   --# return Result =>
   --#    (Left = Right                       -> Result = False) and
   --#    (Left.Last_Index > Right.Last_Index -> Result = False) and
   --#    (Left.Last_Index < Right.Last_Index -> Result = True);
   --  INCOMPLETE
   is
      Result : Boolean;
      Index  : Limb_Index;
   begin
      if Left = Right then
         Result := False;
      elsif Left.Last_Index > Right.Last_Index then
         Result := False;
      elsif Left.Last_Index < Right.Last_Index then
         Result := True;
      else
         Index := Left.Last_Index;
         while Left.Limbs (Index) = Right.Limbs (Index)
           and Index > Limb_Index'First
         loop

            --# assert
            --#    Left /= Right                      and
            --#    Left.Last_Index = Right.Last_Index and
            --#    Index > Limb_Index'First           and
            --#    (for all M in Limb_Index range Index .. Left.Last_Index
            --#       => (Left.Limbs (M) = Right.Limbs (M)));

            Index := Index - 1;
         end loop;

         if Left.Limbs (Index) < Right.Limbs (Index) then
            Result := True;
         else
            Result := False;
         end if;
      end if;

      return Result;
   end Less_Than;

   -------------------------------------------------------------------------

   function Less_Than_Limb
     (Left  : Big_Unsigned;
      Right : Limb_Type)
      return Boolean
   --# return Result =>
   --#    (Left.Last_Index > 0 -> Result = False) and
   --#    (Result -> Left.Limbs (Left.Last_Index) < Right);
   is
      Result : Boolean;
   begin
      if Left.Last_Index > 0 then
         Result := False;
      else
         Result := Left.Limbs (Left.Last_Index) < Right;
      end if;

      return Result;
   end Less_Than_Limb;

   -------------------------------------------------------------------------

   function Greater_Than (Left, Right : Big_Unsigned) return Boolean
   --# return (Left /= Right) and not Less_Than (Left, Right);
   is
   begin
      return (Left /= Right and then
        not Less_Than
          (Left  => Left,
           Right => Right));
   end Greater_Than;

   -------------------------------------------------------------------------

   function To_Bignum (Bytes : Byte_Array) return Big_Unsigned
   is
      type Shift_Type is mod Bytes_Per_Limb;

      Result        : Big_Unsigned;
      Shift_Amount  : Shift_Type := 0;
      Current_Index : Limb_Index;
   begin
      Clear (Number => Result);

      for I in reverse Byte_Index loop
         Current_Index := (Byte_Index'Last - I) / Bytes_Per_Limb;

         Result.Limbs (Current_Index) :=
           Result.Limbs (Current_Index) or
           Limb_Type (SR.Shift_Left_32
             (Value  => Interfaces.Unsigned_32 (Bytes (I)),
              Amount => Natural (Shift_Amount) * Byte'Size));

         Shift_Amount := Shift_Amount + 1;
      end loop;

      Set_Last_Index (Number => Result);

      return Result;
   end To_Bignum;

   -------------------------------------------------------------------------

   function Sub (Left, Right : Big_Unsigned) return Big_Unsigned
   is
      Result : Big_Unsigned := Big_Unsigned_Zero;
      Carry  : Limb_Type    := 0;
   begin
      if Right = Big_Unsigned_Zero then
         Result := Left;
      elsif Right = Left then
         Result := Big_Unsigned_Zero;
      else
         for I in Limb_Index range Limb_Index'First .. Left.Last_Index loop

            --# assert Left = Left%;

            Result.Limbs (I) := (Left.Limbs (I) - Right.Limbs (I)) - Carry;

            if (Right.Limbs (I) > Left.Limbs (I))
              or (Carry = 1 and Right.Limbs (I) = Left.Limbs (I))
            then
               Carry := 1;
            else
               Carry := 0;
            end if;

            if Result.Limbs (I) /= 0 then
               Result.Last_Index := I;
            end if;
         end loop;
      end if;

      return Result;
   end Sub;

   -------------------------------------------------------------------------

   procedure Short_Div
     (Dividend  :     Big_Unsigned;
      Divisor   :     Limb_Type;
      Quotient  : out Big_Unsigned;
      Remainder : out Limb_Type)
   is
      Carry         : Limb_Type;
      Temp          : LU;
      Temp_Quotient : Big_Unsigned;
   begin
      if Less_Than_Limb
        (Left  => Dividend,
         Right => Divisor)
      then
         Remainder := Dividend.Limbs (0);
         Quotient  := Big_Unsigned_Zero;
      elsif Divisor = 1 then
         Quotient  := Dividend;
         Remainder := 0;
      elsif Equal_Limb
        (Left  => Dividend,
         Right => Divisor)
      then
         Quotient  := Big_Unsigned_One;
         Remainder := 0;
      else
         Carry         := 0;
         Temp_Quotient := Big_Unsigned_Zero;

         for I in reverse Limb_Index range Dividend.Limbs'First ..
           Dividend.Last_Index
         loop

            --# assert Carry < Divisor;

            Temp := LU (Dividend.Limbs (I))
              + LU (SR.Shift_Left_64
                    (Value  => Interfaces.Unsigned_64 (Carry),
                     Amount => Limb_Type'Size));
            Temp_Quotient.Limbs (I) := Limb_Type (Temp / LU (Divisor));

            Carry := Limb_Type (Temp mod LU (Divisor));
         end loop;

         if Dividend.Last_Index > 0 and then
           Temp_Quotient.Limbs (Dividend.Last_Index) = 0
         then
            Temp_Quotient.Last_Index := Dividend.Last_Index - 1;
         else
            Temp_Quotient.Last_Index := Dividend.Last_Index;
         end if;

         Quotient  := Temp_Quotient;
         Remainder := Carry;
      end if;
   end Short_Div;

   -------------------------------------------------------------------------

   function Shift_Left
     (Number : Big_Unsigned;
      Amount : Bignum_Size)
      return Big_Unsigned
   is
      subtype Shift_Type is Natural range 0 .. Limb_Type'Size;

      Result   : Big_Unsigned := Big_Unsigned_Zero;
      L, R     : Shift_Type;
      M, Upper : Limb_Index;
      T1, T2   : Interfaces.Unsigned_32;
   begin
      if Amount = Bignum_Size'First
        or Number = Big_Unsigned_Zero
      then
         Result := Number;
      elsif Amount = Bignum_Size'Last then
         Result := Big_Unsigned_Zero;
      else
         L := Amount mod Limb_Type'Size;
         R := Limb_Type'Size - L;
         M := Amount / Limb_Type'Size;
         Upper := Min_Limb_Index
           (Left  => Limb_Index'Last - M,
            Right => Number.Last_Index);

         T1 := SR.Shift_Left_32
           (Value  => Interfaces.Unsigned_32
              (Number.Limbs (Number.Limbs'First)),
            Amount => L);
         Result.Limbs (M) := Limb_Type (T1);

         for I in Limb_Index range Limb_Index'First + 1 .. Upper
         loop

            --# assert
            --#    Upper <= Limb_Index'Last - M and
            --#    I <= Upper                   and
            --#    Upper = Upper%               and
            --#    M     = M%                   and
            --#    M in Limb_Index              and
            --#    Amount > Bignum_Size'First   and
            --#    Amount < Bignum_Size'Last    and
            --#    Number /= Big_Unsigned_Zero  and
            --#    Number = Number%             and
            --#    L in Shift_Type and R in Shift_Type;

            T1 := SR.Shift_Right_32
              (Value  => Interfaces.Unsigned_32 (Number.Limbs (I - 1)),
               Amount => R);
            T2 := SR.Shift_Left_32
              (Value  => Interfaces.Unsigned_32 (Number.Limbs (I)),
               Amount => L);

            Result.Limbs (I + M) := Limb_Type (T1 xor T2);
         end loop;

         if Upper + M /= Limb_Index'Last then

            T1 := SR.Shift_Right_32
              (Value  => Interfaces.Unsigned_32
                 (Number.Limbs (Number.Last_Index)),
               Amount => R);

            Result.Limbs ((Upper + M) + 1) := Limb_Type (T1);
         end if;

         Set_Last_Index (Number => Result);
      end if;

      return Result;
   end Shift_Left;

   -------------------------------------------------------------------------

   function Mul_Limb
     (Left  : Big_Unsigned;
      Right : Limb_Type)
      return Big_Unsigned
   is
      Upper      : Limb_Index;
      Carry      : Limb_Type := 0;
      Temp       : LU;
      Temp_Array : Limb_Array_Plus_One := Limb_Array_Plus_One'(others => 0);
      Result     : Big_Unsigned        := Big_Unsigned_Zero;
   begin
      if Right = 0 or Left = Big_Unsigned_Zero then
         Result := Big_Unsigned_Zero;
      elsif Left = Big_Unsigned_One then
         Result.Limbs (Limb_Index'First) := Right;
         Result.Last_Index := Limb_Index'First;
      elsif Right = 1 then
         Result.Limbs := Left.Limbs;
         Result.Last_Index := Limb_Index'First;
      else
         Upper := Left.Last_Index;

         for I in Limb_Index range Limb_Index'First .. Upper loop

            --# assert Upper in Limb_Index;

            Temp := LU (Left.Limbs (I)) * LU (Right) + LU (Carry);
            Temp_Array (I) := Limb_Type (Temp mod (LU (Limb_Type'Last) + 1));
            Carry := Limb_Type (Temp / (LU (Limb_Type'Last) + 1));
         end loop;

         if Carry > 0 then
            Temp_Array (Upper + 1) := Carry;
         end if;

         for I in Limb_Index loop
            Result.Limbs (I) := Temp_Array (I);
         end loop;

         Set_Last_Index (Number => Result);
      end if;

      return Result;
   end Mul_Limb;

   -------------------------------------------------------------------------

   procedure Long_Div
     (Dividend  :     Big_Unsigned;
      Divisor   :     Big_Unsigned;
      Quotient  : out Big_Unsigned;
      Remainder : out Big_Unsigned)
   is
      subtype Scale_Shift_Type is Limb_Bit_Size range
        Limb_Bit_Size'First .. Limb_Bit_Size'Last - 1;

      UN                  : Limb_Array_Plus_One;
      VN                  : Limb_Array;
      Q_Hat, R_Hat, P     : LU;
      T_Rem, T_Div, Carry : Limb_Type;
      T                   : LS;
      Scaling_Factor      : Scale_Shift_Type;
      N, M                : Limb_Index_Plus_One;

      Radix : constant Radix_Type := Radix_Type'Last;

      ----------------------------------------------------------------------

      function To_Limb (Number : LS) return Limb_Type
      is
         Result : Limb_Type;
         Temp   : LS;
      begin
         Temp := Number mod LS (Radix);

         if Temp < 0 then
            Result := Limb_Type (LS (Radix) - Temp);
         else
            Result := Limb_Type (Temp);
         end if;

         return Result;
      end To_Limb;

      ----------------------------------------------------------------------

      procedure Normalize_Divisor
        (Number :     Big_Unsigned;
         Result : out Limb_Array;
         Factor : out Scale_Shift_Type)
      --# derives
      --#    Result, Factor from Number;
      --# pre
      --#    Number /= Big_Unsigned_Zero and
      --#    Number.Last_Index >= 1;
      --# post
      --#    Result (Number.Last_Index) /= 0;
      is
         Bit_Len : Limb_Bit_Size;
         T1, T2  : Interfaces.Unsigned_32;
      begin
         Result := Limb_Array'(others => 0);

         --# assert Number.Limbs (Number.Last_Index) > 0;

         Bit_Len := Bit_Length_Limb
           (Number => Number.Limbs (Number.Last_Index));

         --# assert
         --#    Number.Limbs (Number.Last_Index) > 0  and
         --#    Bit_Len > 0                           and
         --#    Radix_Type'(2 ** Bit_Len) > Radix_Type
         --#       (Number.Limbs (Number.Last_Index)) and
         --#    Limb_Type'(2 ** (Bit_Len - 1)) <= Number.Limbs
         --#       (Number.Last_Index);

         Factor := Limb_Bit_Size'Last - Bit_Len;

         for I in reverse Limb_Index range
           Limb_Index'First + 1 .. Number.Last_Index - 1
         loop
            T1 := SR.Shift_Left_32
              (Value  => Interfaces.Unsigned_32 (Number.Limbs (I)),
               Amount => Factor);
            T2 := SR.Shift_Right_32
              (Value  => Interfaces.Unsigned_32 (Number.Limbs (I - 1)),
               Amount => Word_Size - Factor);

            Result (I) := Limb_Type (T1 or T2);

            --# assert
            --#    Number.Limbs (Number.Last_Index) > 0 and
            --#    2 ** Bit_Len * 2 ** Factor = Radix   and
            --#    2 ** Factor > 0                      and
            --#    Radix_Type (Number.Limbs (Number.Last_Index)) * 2 ** Factor
            --#       < Radix;

         end loop;

         Result (Limb_Index'First) := Limb_Type (SR.Shift_Left_32
           (Value  => Interfaces.Unsigned_32 (Number.Limbs (Limb_Index'First)),
            Amount => Factor));

         T1 := SR.Shift_Left_32
           (Value  => Interfaces.Unsigned_32
              (Number.Limbs (Number.Last_Index)),
            Amount => Factor);

         --# check
         --#    2 ** Bit_Len * 2 ** Factor = Radix and
         --#    2 ** Factor > 0                    and
         --#    Radix_Type (Number.Limbs (Number.Last_Index)) * 2 ** Factor
         --#       < Radix;

         --# assert T1 > 0;

         T2 := SR.Shift_Right_32
           (Value  => Interfaces.Unsigned_32
              (Number.Limbs (Number.Last_Index - 1)),
            Amount => Word_Size - Factor);

         Result (Number.Last_Index) := Limb_Type (T1 or T2);

      end Normalize_Divisor;

      ----------------------------------------------------------------------

      function Shift_Left_Plus_One
        (Number : Big_Unsigned;
         Amount : Scale_Shift_Type)
         return Limb_Array_Plus_One
      is
         T1, T2 : Interfaces.Unsigned_32;
         M      : Limb_Index_Plus_One;
         Result : Limb_Array_Plus_One := Limb_Array_Plus_One'(others => 0);
      begin
         M := Number.Last_Index + 1;

         T1 := SR.Shift_Right_32
           (Value  => Interfaces.Unsigned_32 (Number.Limbs (M - 1)),
            Amount => Word_Size - Amount);
         Result (M) := Limb_Type (T1);

         for I in reverse Limb_Index range Limb_Index'First + 1 .. M - 1
         loop
            T1 := SR.Shift_Left_32
              (Value  => Interfaces.Unsigned_32 (Number.Limbs (I)),
               Amount => Amount);
            T2 := SR.Shift_Right_32
              (Value  => Interfaces.Unsigned_32 (Number.Limbs (I - 1)),
               Amount => Word_Size - Amount);

            Result (I) := Limb_Type (T1 or T2);
         end loop;

         T1 := SR.Shift_Left_32
           (Value  => Interfaces.Unsigned_32 (Number.Limbs (Limb_Index'First)),
            Amount => Amount);
         Result (Limb_Index'First) := Limb_Type (T1);

         return Result;
      end Shift_Left_Plus_One;

   begin
      Quotient := Big_Unsigned_Zero;

      if Divisor.Last_Index = 0 then
         case Divisor.Limbs (Limb_Index'First) is
            when 1 =>
               Quotient  := Dividend;
               Remainder := Big_Unsigned_Zero;
            when others =>
               T_Div := Divisor.Limbs (Limb_Index'First);
               Short_Div (Dividend  => Dividend,
                          Divisor   => T_Div,
                          Quotient  => Quotient,
                          Remainder => T_Rem);
               Remainder := Big_Unsigned'
                 (Last_Index => 0,
                  Limbs      => Limb_Array'
                    (Limb_Index'First => T_Rem,
                     others           => 0));
         end case;

      elsif Less_Than
        (Left  => Dividend,
         Right => Divisor)
      then
         Quotient  := Big_Unsigned_Zero;
         Remainder := Dividend;

      elsif Dividend = Big_Unsigned_Zero then
         Quotient  := Big_Unsigned_Zero;
         Remainder := Big_Unsigned_Zero;

      elsif Bit_Length (Dividend) = Bit_Length (Divisor) then

         --# assert
         --#    Dividend = Divisor or
         --#    not Less_Than (Dividend, Divisor);

         Quotient  := Big_Unsigned_One;
         Remainder := Sub (Left  => Dividend,
                           Right => Divisor);

      else

         M := Dividend.Last_Index + 1;
         N := Divisor.Last_Index + 1;

         --  D1, Normalize

         Normalize_Divisor
           (Number => Divisor,
            Result => VN,
            Factor => Scaling_Factor);
         UN := Shift_Left_Plus_One
           (Number => Dividend,
            Amount => Scaling_Factor);

         --  D2, Initialize J

         for J in reverse Limb_Index range Limb_Index'First .. M - N
         loop

            --  D3, Calculate estimates (qhat and rhat)

            --# assert
            --#    VN (N - 1) /= 0                           and
            --#    Dividend.Last_Index >= Divisor.Last_Index and
            --#    M - 1 in Limb_Index                       and
            --#    N - 2 in Limb_Index                       and
            --#    M - N in Limb_Index                       and
            --#    J + N in Limb_Index_Plus_One              and
            --#    J in Limb_Index'First .. M - N;

            Q_Hat := (LU (UN (J + N)) * LU (Radix)) + LU (UN (J + (N - 1)));
            Q_Hat := Q_Hat / LU (VN (N - 1));

            R_Hat := (LU (UN (J + N)) * LU (Radix)) + LU (UN (J + (N - 1)));
            R_Hat := R_Hat - (Q_Hat * LU (VN (N - 1)));

            while Q_Hat >= LU (Radix) or
              (Q_Hat * LU (VN (N - 2))) >
              ((LU (Radix) * R_Hat) + LU (UN (J + (N - 2))))
            loop
               Q_Hat := Q_Hat - 1;
               R_Hat := R_Hat + LU (VN (N - 1));

               if R_Hat >= LU (Radix) then
                  exit;
               end if;

               --# assert
               --#    VN (N - 1) /= 0                           and
               --#    Dividend.Last_Index >= Divisor.Last_Index and
               --#    M - 1 in Limb_Index                       and
               --#    N - 2 in Limb_Index                       and
               --#    M - N in Limb_Index                       and
               --#    J + N in Limb_Index_Plus_One              and
               --#    J in Limb_Index'First .. M - N            and
               --#    R_Hat < LU (Radix);

            end loop;

            --  D4, Multiply and substract

            Carry := 0;
            for I in Limb_Index range Limb_Index'First .. N - 1 loop

               --# assert
               --#    VN (N - 1) /= 0                           and
               --#    Dividend.Last_Index >= Divisor.Last_Index and
               --#    M - 1 in Limb_Index                       and
               --#    N - 2 in Limb_Index                       and
               --#    M - N in Limb_Index                       and
               --#    J + N in Limb_Index_Plus_One              and
               --#    J in Limb_Index'First .. M - N            and
               --#    N = N%                                    and
               --#    I in Limb_Index'First .. N - 1            and
               --#    I + J in Limb_Index_Plus_One              and
               --#    Q_Hat * LU (VN (I)) / (2 ** Word_Size)
               --#       <= LU (Limb_Type'Last);

               P := Q_Hat * LU (VN (I));
               T := (LS (UN (I + J)) - LS (Carry)) -
                 LS (P and LU (Limb_Type'Last));

               UN (I + J) := To_Limb (Number => T);

               --# check P / 2 ** Word_Size >= LU (T / 2 ** Word_Size);

               Carry := Limb_Type (LS (SR.Shift_Right_64
                 (Value  => Interfaces.Unsigned_64 (P),
                  Amount => Word_Size)) - Shift_Right_Arithmetic
                   (Value  => T,
                    Amount => Word_Size));
            end loop;

            T := LS (UN (J + N)) - LS (Carry);
            UN (J + N) := To_Limb (Number => T);

            --# check Q_Hat <= LU (Limb_Type'Last);

            Quotient.Limbs (J) := Limb_Type (Q_Hat);

            --  D5, Test remainder

            if T < 0 then

               --  D6, Add back

               Quotient.Limbs (J) := Quotient.Limbs (J) - 1;

               Carry := 0;
               for I in Limb_Index range Limb_Index'First .. N - 1
               loop

                  --# assert
                  --#    VN (N - 1) /= 0                           and
                  --#    Dividend.Last_Index >= Divisor.Last_Index and
                  --#    M - 1 in Limb_Index                       and
                  --#    N - 2 in Limb_Index                       and
                  --#    M - N in Limb_Index                       and
                  --#    J + N in Limb_Index_Plus_One              and
                  --#    J in Limb_Index'First .. M - N            and
                  --#    N = N%                                    and
                  --#    I in Limb_Index'First .. N - 1            and
                  --#    I + J in Limb_Index_Plus_One;

                  T := (LS (UN (I + J)) + LS (VN (I))) + LS (Carry);
                  UN (I + J) := To_Limb (Number => T);

                  --# check
                  --#    T / 2 ** Word_Size >= LS (Limb_Type'First) and
                  --#    T / 2 ** Word_Size <= LS (Limb_Type'Last);

                  Carry := Limb_Type (Shift_Right_Arithmetic
                    (Value  => T,
                     Amount => Word_Size));
               end loop;

               UN (J + N) := UN (J + N) + Carry;
            end if;

            --  D7, Loop on J

         end loop;

         --  D8, Unnormalize

         Remainder := Big_Unsigned_Zero;
         for I in Limb_Index range Limb_Index'First .. N - 1 loop
            Remainder.Limbs (I) := Limb_Type
              (SR.Shift_Right_32
                 (Value  => Interfaces.Unsigned_32 (UN (I)),
                  Amount => Scaling_Factor) or
                 SR.Shift_Left_32
                   (Value  => Interfaces.Unsigned_32 (UN (I + 1)),
                    Amount => Word_Size - Scaling_Factor));
         end loop;

         Set_Last_Index (Number => Quotient);
         Set_Last_Index (Number => Remainder);
      end if;
   end Long_Div;

   -------------------------------------------------------------------------

   function Mul (Left, Right : Big_Unsigned) return Big_Unsigned
   is
      subtype Double_Index is Natural range
        Limb_Index'First .. 2 * (Limb_Index'Last + 1);

      type Double_Limb_Array is array (Double_Index) of Limb_Type;

      T          : LU;
      Carry      : Limb_Type         := 0;
      Temp_Array : Double_Limb_Array := Double_Limb_Array'(others => 0);
      Result     : Big_Unsigned      := Big_Unsigned_Zero;

      Left_Last, Right_Last : Limb_Index;
   begin
      if Right = Big_Unsigned_Zero or Left = Big_Unsigned_Zero then
         Result := Big_Unsigned_Zero;
      elsif Right = Big_Unsigned_One then
         Result := Left;
      elsif Left = Big_Unsigned_One then
         Result := Right;
      else
         Left_Last  := Left.Last_Index;
         Right_Last := Right.Last_Index;

         for I in Limb_Index range Limb_Index'First .. Left_Last loop

            for J in Limb_Index range Limb_Index'First .. Right_Last loop

               --# assert I + J in Double_Index;

               T := LU (Left.Limbs (I))
                 * LU (Right.Limbs (J))
                 + LU (Temp_Array (I + J))
                 + LU (Carry);

               Temp_Array (I + J) := Limb_Type
                 (T mod (LU (Limb_Type'Last) + 1));

               Carry := Limb_Type (T / (LU (Limb_Type'Last) + 1));

            end loop;

            --# assert (I + Right.Last_Index) + 1 <= Double_Index'Last;

            Temp_Array ((I + Right.Last_Index) + 1) :=
              Carry + Temp_Array ((I + Right.Last_Index) + 1);

            Carry := 0;
         end loop;

         for I in Limb_Index loop
            Result.Limbs (I) := Temp_Array (I);
         end loop;

         Set_Last_Index (Number => Result);
      end if;

      return Result;
   end Mul;

   -------------------------------------------------------------------------

   function Mod_Limb
     (Left  : Big_Unsigned;
      Right : Limb_Type)
      return Big_Unsigned
   is
      Q : Big_Unsigned;
      R : Limb_Type;
   begin

      --# assert Right /= 0;

      --# accept Flow, 10, Q, "Quotient Q not required to calculate remainder";

      Short_Div (Dividend  => Left,
                 Divisor   => Right,
                 Quotient  => Q,
                 Remainder => R);

      --# accept Flow, 33, Q, "Value of Q not required to create final result";

      return Big_Unsigned'
        (Last_Index => Limb_Index'First,
         Limbs      => Limb_Array'
           (0      => R,
            others => 0));
   end Mod_Limb;

end Big_Numbers;
