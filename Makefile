PWD      = $(shell pwd)
OUT_DIR  = $(PWD)/out
OBJ_DIR  = obj
RUL_DIR  = $(PWD)/rules/big_numbers
PREPARE := $(shell mkdir -p $(OUT_DIR))

NUM_CPUS := $(shell getconf _NPROCESSORS_ONLN)

POGS_SUM      = bignum.sum
SPARK_DIR    ?= /opt/spark
SPARK_OPTIONS = \
	-vcg \
	-brief \
	-error_explanations=first \
	-config_file=$(OUT_DIR)/target.cfg \
	-warning_file=warnings.cfg \
	-ou=$(OUT_DIR)

all: meta target_config verify

meta: $(OUT_DIR)/bignum.idx $(OUT_DIR)/bignum.smf

target_config: $(OUT_DIR)/target.cfg

$(OUT_DIR)/bignum.idx $(OUT_DIR)/bignum.smf:
	@(cd src && sparkmake -include=* -index=$(OUT_DIR)/bignum.idx \
		-meta=$(OUT_DIR)/bignum.smf)

$(OUT_DIR)/confgen: $(SPARK_DIR)/lib/spark/confgen.adb
	@gnatmake -o $@ -D $(OUT_DIR) $^

$(OUT_DIR)/target.cfg: $(OUT_DIR)/confgen
	$^ > $@

verify:
	@spark $(SPARK_OPTIONS) -i=$(OUT_DIR)/bignum.idx @$(OUT_DIR)/bignum.smf
	@cp -R $(RUL_DIR) $(OUT_DIR)
	@sparksimp -p=$(NUM_CPUS)
	@pogs -o=$(POGS_SUM)
	@cat $(POGS_SUM)

tests: build_tests
	@$(OBJ_DIR)/runner

build_tests:
	@gnatmake -p -Pbignum_tests

clean:
	@rm -rf $(OUT_DIR) $(OBJ_DIR) $(POGS_SUM)
