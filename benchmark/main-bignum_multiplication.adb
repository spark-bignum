-------------------------------------------------------------------------------
-- This file is part of libsparkcrypto.
--
-- Copyright (C) 2010, Reto Buerki
-- Copyright (C) 2010, Adrian-Ken Rueegsegger
-- Copyright (C) 2010, secunet Security Networks AG
-- All rights reserved.
--
-- Redistribution  and  use  in  source  and  binary  forms,  with  or  without
-- modification, are permitted provided that the following conditions are met:
--
--    * Redistributions of source code must retain the above copyright notice,
--      this list of conditions and the following disclaimer.
--
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
--    * Neither the name of the  nor the names of its contributors may be used
--      to endorse or promote products derived from this software without
--      specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE  COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY  EXPRESS OR IMPLIED WARRANTIES,  INCLUDING, BUT NOT LIMITED  TO, THE
-- IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR  A PARTICULAR PURPOSE
-- ARE  DISCLAIMED. IN  NO EVENT  SHALL  THE COPYRIGHT  HOLDER OR  CONTRIBUTORS
-- BE  LIABLE FOR  ANY  DIRECT, INDIRECT,  INCIDENTAL,  SPECIAL, EXEMPLARY,  OR
-- CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT  OF
-- SUBSTITUTE GOODS  OR SERVICES; LOSS  OF USE,  DATA, OR PROFITS;  OR BUSINESS
-- INTERRUPTION)  HOWEVER CAUSED  AND ON  ANY THEORY  OF LIABILITY,  WHETHER IN
-- CONTRACT,  STRICT LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE OR  OTHERWISE)
-- ARISING IN ANY WAY  OUT OF THE USE OF THIS SOFTWARE, EVEN  IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

with Interfaces.C;

with GMP.Binding;

with Big_Numbers.Debug;

separate (Main)
procedure Bignum_Multiplication
is
   Retval : Interfaces.C.int;
   M      : SPARKUnit.Measurement_Type;

   BN1, BN_Result    : Big_Numbers.Big_Unsigned;
   GMPN1, GMP_Result : GMP.Binding.Mpz_T;
begin
   GMP.Binding.Mpz_Init (Integer => GMP_Result);
   GMP.Binding.Mpz_Init_Set_Str
     (Result => Retval,
      Rop    => GMPN1,
      Str    => Interfaces.C.To_C (Number_1_Str),
      Base   => 10);

   SPARKUnit.Reference_Start (Item => M);
   for I in Iterations loop
      GMP.Binding.Mpz_Mul
        (Rop => GMP_Result,
         Op1 => GMPN1,
         Op2 => GMPN1);
   end loop;
   SPARKUnit.Reference_Stop (Item => M);

   BN1 := Get_BN1;
   SPARKUnit.Measurement_Start (Item => M);
   for I in Iterations loop
      BN_Result := Big_Numbers.Mul
        (Left  => BN1,
         Right => BN1);
   end loop;
   SPARKUnit.Measurement_Stop (Item => M);

   declare
      GMP_Str : Interfaces.C.char_array (0 .. 60);
      BN_Str  : constant String := Big_Numbers.Debug.To_String
        (Number => BN_Result);
   begin
      GMP.Binding.Mpz_Get_Str
        (Str  => GMP_Str,
         Base => 10,
         Op   => GMP_Result);

      SPARKUnit.Create_Benchmark
        (Harness     => Harness,
         Suite       => Benchmarks,
         Description => "BN = BN * BN",
         Measurement => M,
         Success     => Interfaces.C.To_Ada (GMP_Str) = BN_Str);
   end;
end Bignum_Multiplication;
