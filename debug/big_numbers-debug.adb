-------------------------------------------------------------------------------
-- This file is part of libsparkcrypto.
--
-- Copyright (C) 2010, Reto Buerki
-- Copyright (C) 2010, Adrian-Ken Rueegsegger
-- Copyright (C) 2010, secunet Security Networks AG
-- All rights reserved.
--
-- Redistribution  and  use  in  source  and  binary  forms,  with  or  without
-- modification, are permitted provided that the following conditions are met:
--
--    * Redistributions of source code must retain the above copyright notice,
--      this list of conditions and the following disclaimer.
--
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
--    * Neither the name of the  nor the names of its contributors may be used
--      to endorse or promote products derived from this software without
--      specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE  COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY  EXPRESS OR IMPLIED WARRANTIES,  INCLUDING, BUT NOT LIMITED  TO, THE
-- IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR  A PARTICULAR PURPOSE
-- ARE  DISCLAIMED. IN  NO EVENT  SHALL  THE COPYRIGHT  HOLDER OR  CONTRIBUTORS
-- BE  LIABLE FOR  ANY  DIRECT, INDIRECT,  INCIDENTAL,  SPECIAL, EXEMPLARY,  OR
-- CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT  OF
-- SUBSTITUTE GOODS  OR SERVICES; LOSS  OF USE,  DATA, OR PROFITS;  OR BUSINESS
-- INTERRUPTION)  HOWEVER CAUSED  AND ON  ANY THEORY  OF LIABILITY,  WHETHER IN
-- CONTRACT,  STRICT LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE OR  OTHERWISE)
-- ARISING IN ANY WAY  OUT OF THE USE OF THIS SOFTWARE, EVEN  IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

with Ada.Strings.Unbounded;

package body Big_Numbers.Debug is

   -------------------------------------------------------------------------

   function To_String (Number : Big_Unsigned) return String
   is
      use Ada.Strings.Unbounded;

      subtype Number_Index is Limb_Index range 0 .. 9;
      type Trans_Array is array (Number_Index) of Character;
      Trans : constant Trans_Array :=
        ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

      Temp_Num  : Big_Unsigned := Number;
      Remainder : Limb_Type    := 0;
      Result    : Unbounded_String;
   begin
      if Number = Big_Unsigned_Zero then
         return "0";
      end if;

      while not Equal
        (Left  => Temp_Num,
         Right => Big_Unsigned_Zero)
      loop
         Short_Div (Dividend  => Temp_Num,
                    Divisor   => 10,
                    Quotient  => Temp_Num,
                    Remainder => Remainder);

         Result := Trans (Number_Index (Remainder)) & Result;
      end loop;

      return To_String (Result);
   end To_String;

end Big_Numbers.Debug;
